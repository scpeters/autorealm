README
======

AutoREALM is a drawing software mostly oriented to create RPG maps and dungeons, targeting people with no drawing or computer knowledge.
This software was written for Windows, in Delphi and was not maintained from many years, so the decision was made to rewrite it with more
portable technologies, like C++, openGL and wxWidgets. Because autoREALM has had many attempts to rewrite it and because they all failed, resulting in
undocumented, hard to read code, a real effort is made in this rewrite to keep a very clean base code and a clean architecture. To make 
things easier to potential contributors, this rewrite also uses a plug-in architecture. For more details about features of that soft, please go on the official
website of the actual stable version: http://autorealm.sourceforge.net/.

Please note that this rewrite is on alpha stage and under heavy development.

Required Libraries
==================
	* boost-* (Version 1.49)
	* openGL (freeglut3 version 2.6.0 or greater) (http://www.opengl.org/)
	* Pluma-fork (Current Version) (https://bitbucket.org/bmorel/pluma-fork)
	* tree template library (Current Version) (https://bitbucket.org/bmorel/tree
	* wxWidgets (version 2.9 or greater) (http://www.wxwidgets.org/)

Misc Requirements
=================
	* This project can be built with either the Code::Blocks IDE or cmake
	* autoRealm requires the tree template library to be copied into its /src directory
