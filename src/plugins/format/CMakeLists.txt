find_package(wxWidgets REQUIRED )
include(${wxWidgets_USE_FILE})

find_package(Boost 1.39 COMPONENTS serialization REQUIRED)

include_directories(${PROJECT_SOURCE_DIR}/src/renderEngine)

subdirs(load)
subdirs(loadxml)
subdirs(save)
subdirs(savexml)
